
struct ph_socket_driver {
	void			*priv;
	int			pri, nt;
	ph_socket_t		ph_socket;
	struct dchannel		*dch;
	int 			enabled, activated;
};

int init_ph_socket_driver(struct ph_socket_driver *drv, void *priv, const char *socket_name, int pri, int nt, uint32_t debug);
void exit_ph_socket_driver(struct ph_socket_driver *drv);

void bchannel_ph_sock_receive(void *priv, int channel, uint8_t prim, uint8_t *data, int length);

