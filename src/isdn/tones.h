
struct isdn_tone {
        int             tone;
        void            *pattern;
        int             count;
        int             index;
};

void isdn_tone_generate_ulaw_samples(void);
void isdn_tone_copy(struct isdn_tone *t, uint8_t *data, int len);
int isdn_tone_set(struct isdn_tone *t, int tone);


