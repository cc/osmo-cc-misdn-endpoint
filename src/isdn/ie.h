
#define CHANNEL_NO		-2	/* incoming call on hold */
#define CHANNEL_ANY		-3	/* give me any channel */
#define CHANNEL_FREE		-100	/* select channel that is free */

void enc_ie_complete(struct l3_msg *l3m, int complete);
void dec_ie_complete(struct l3_msg *l3m, int *complete);
void enc_ie_bearer(struct l3_msg *l3m, uint8_t coding, uint8_t capability, int has_mode, uint8_t mode, uint8_t rate, int has_multi, uint8_t multi, int has_user, uint8_t user);
int dec_ie_bearer(struct l3_msg *l3m, uint8_t *coding, uint8_t *capability, int *has_mode, uint8_t *mode, uint8_t *rate, int *has_multi, uint8_t *multi, int *has_user, uint8_t *user);
void enc_ie_hlc(struct l3_msg *l3m, uint8_t coding, uint8_t interpretation, uint8_t presentation, uint8_t hlc, int has_exthlc, uint8_t exthlc);
int dec_ie_hlc(struct l3_msg *l3m, uint8_t *coding, uint8_t *interpretation, uint8_t *presentation, uint8_t *hlc, int *has_exthlc, uint8_t *exthlc);
void enc_ie_call_id(struct l3_msg *l3m, uint8_t *callid, int callid_len);
int dec_ie_call_id(struct l3_msg *l3m, uint8_t *callid, int *callid_len);
void enc_ie_called_pn(struct l3_msg *l3m, uint8_t type, uint8_t plan, char *number);
int dec_ie_called_pn(struct l3_msg *l3m, uint8_t *type, uint8_t *plan, char *number, int number_len);
void enc_ie_calling_pn(struct l3_msg *l3m, uint8_t type, uint8_t plan, int has_presen, uint8_t present, uint8_t screen, char *number);
int dec_ie_calling_pn(struct l3_msg *l3m, int secondary_ie, uint8_t *type, uint8_t *plan, int *has_present, uint8_t *present, uint8_t *screen, char *number, int number_len);
void enc_ie_connected_pn(struct l3_msg *l3m, uint8_t type, uint8_t plan, int has_present, uint8_t present, uint8_t screen, char *number);
int dec_ie_connected_pn(struct l3_msg *l3m, uint8_t *type, uint8_t *plan, int *has_present, uint8_t *present, uint8_t *screen, char *number, int number_len);
void enc_ie_cause(struct l3_msg *l3m, uint8_t location, uint8_t cause);
int dec_ie_cause(struct l3_msg *l3m, uint8_t *location, uint8_t *cause);
void enc_ie_channel_id(struct l3_msg *l3m, int pri, int exclusive, int channel);
int dec_ie_channel_id(struct l3_msg *l3m, int pri, int *exclusive, int *channel);
void enc_ie_date(struct l3_msg *l3m, time_t ti, int no_seconds);
void enc_ie_display(struct l3_msg *l3m, char *display);
int dec_ie_display(struct l3_msg *l3m, char *display, int display_len);
void enc_ie_keypad(struct l3_msg *l3m, char *keypad);
int dec_ie_keypad(struct l3_msg *l3m, char *keypad, int keypad_len);
void enc_ie_notify(struct l3_msg *l3m, uint8_t notify);
int dec_ie_notify(struct l3_msg *l3m, uint8_t *notify);
void enc_ie_progress(struct l3_msg *l3m, uint8_t coding, uint8_t location, uint8_t progress);
int dec_ie_progress(struct l3_msg *l3m, uint8_t *coding, uint8_t *location, uint8_t *progress);
void enc_ie_redirecting(struct l3_msg *l3m, uint8_t type, uint8_t plan, int has_present, uint8_t present, uint8_t screen, int has_reason, uint8_t reason, char *number);
int dec_ie_redirecting(struct l3_msg *l3m, uint8_t *type, uint8_t *plan, int *has_present, uint8_t *present, uint8_t *screen, int *has_reason, uint8_t *reason, char *number, int number_len);
void enc_ie_redirection(struct l3_msg *l3m, uint8_t type, uint8_t plan, int has_present, uint8_t present, char *number);
int dec_ie_redirection(struct l3_msg *l3m, uint8_t *type, uint8_t *plan, int *has_present, uint8_t *present, char *number, int number_len);
void enc_ie_facility(struct l3_msg *l3m, uint8_t *facility, int facility_len);
int dec_ie_facility(struct l3_msg *l3m, uint8_t *facility, int *facility_len);
void enc_ie_useruser(struct l3_msg *l3m, uint8_t protocol, uint8_t *user, int user_len);
int dec_ie_useruser(struct l3_msg *l3m, uint8_t *protocol, uint8_t *user, int *user_len);
void enc_ie_signal(struct l3_msg *l3m, uint8_t signal);

