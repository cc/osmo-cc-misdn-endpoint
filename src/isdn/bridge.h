
int bridge_socket_server(int slots);
void bridge_socket_server_child(int slots, int daemon);

int bridge_socket_client(isdn_t *isdn_ep);
void bridge_socket_client_update(call_t *call, int enable);

