
#ifndef M_BITOPS_H
#define M_BITOPS_H

static inline int test_bit(unsigned int nr, unsigned long *addr)
{
	return 1UL & (*addr >> nr);
}

static inline int test_and_set_bit(unsigned int nr, unsigned long *addr)
{
	unsigned long old = *addr;
	*addr |= 1UL << nr;
	return 1UL & (old >> nr);
}

static inline int test_and_clear_bit(unsigned int nr, unsigned long *addr)
{
	unsigned long old = *addr;
	*addr &= ~(1UL << nr);
	return 1UL & (old >> nr);
}

#endif

