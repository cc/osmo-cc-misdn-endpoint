
#ifndef _MISDN_TIMER_H
#define _MISDN_TIMER_H

#include <osmocom/core/timer.h>

#if 0
#define timer_list timer
#define timer_setup(ti, fu, flags) timer_init(ti, fu, NULL)
#define add_timer osmo_timer_schedule
#define del_timer osmo_timer_del
#define timer_pending osmo_timer_pending
#endif

#define from_timer(var, callback_timer, timer_fieldname) \
        container_of(callback_timer, typeof(*var), timer_fieldname)

#endif

