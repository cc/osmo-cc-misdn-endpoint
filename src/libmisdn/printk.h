
#ifndef MISDN_PRINTK_H
#define MISDN_PRINTK_H

#include <stdint.h>
#include "../liblogging/logging.h"

#define KERN_EMERG	"0"	/* system is unusable */
#define KERN_ALERT	"1"	/* action must be taken immediately */
#define KERN_CRIT	"2"	/* critical conditions */
#define KERN_ERR	"3"	/* error conditions */
#define KERN_WARNING	"4"	/* warning conditions */
#define KERN_NOTICE	"5"	/* normal but significant condition */
#define KERN_INFO	"6"	/* informational */
#define KERN_DEBUG	"7"	/* debug-level messages */

#define printk(fmt, arg...) _printk(__FILE__, __FUNCTION__, __LINE__, fmt, ## arg)
void _printk(const char *file, const char *function, int line, const char *fmt, ...) __attribute__ ((__format__ (__printf__, 4, 5)));

#endif
