/* mlist.h
 *
 * Simple doubly linked list implementation and helper
 * function reduce version of linux kernel list.h
 */

#ifndef _MLIST_H
#define _MLIST_H

struct list_head {
	struct list_head *next, *prev;
};

struct m_hlist_head {
	struct m_hlist_node *first;
};

struct m_hlist_node {
	struct m_hlist_node *next, **pprev;
};


#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
	struct list_head name = LIST_HEAD_INIT(name)

static inline void INIT_LIST_HEAD(struct list_head *list)
{
	list->next = list;
	list->prev = list;
}

#define INIT_M_HLIST_HEAD(ptr) ((ptr)->first = NULL)

/*
 * list_add_head - add a new entry on the top
 * @new: new entry to be added
 * @head: list head to add it after
 *
 */
static inline void list_add_head(struct list_head *new, struct list_head *head)
{
	head->next->prev = new;
	new->next = head->next;
	new->prev = head;
	head->next = new;
}

/**
 * m_hlist_add_head - add a new entry at the beginning of the m_hlist
 * @n: new entry to be added
 * @h: m_hlist head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static inline void m_hlist_add_head(struct m_hlist_node *n, struct m_hlist_head *h)
{
        struct m_hlist_node *first = h->first;
        n->next = first;
        if (first)
                first->pprev = &n->next;
        h->first = n;
        n->pprev = &h->first;
}

/*
 * list_add_tail - add a new entry
 * @new: new entry to be added
 * @head: list head to add it before
 *
 */
static inline void list_add_tail(struct list_head *new, struct list_head *head)
{
	head->prev->next = new;
	new->next = head;
	new->prev = head->prev;
	head->prev = new;
}

#define	LIST_POISON1	0xdeadbee1
#define LIST_POISON2	0xdeadbee2

/*
 * list_del - deletes entry from list.
 * @entry: the element to delete from the list.
 */
static inline void list_del(struct list_head *entry)
{
	entry->next->prev = entry->prev;
	entry->prev->next = entry->next;
	entry->next = (void *)LIST_POISON1;
	entry->prev = (void *)LIST_POISON2;
}

static inline void m_hlist_del(struct m_hlist_node *n)
{
        struct m_hlist_node *next = n->next;
        struct m_hlist_node **pprev = n->pprev;

        *pprev = next;
        if (next)
                next->pprev = pprev;
	n->next = (void *)LIST_POISON1;
	n->pprev = (void *)LIST_POISON2;
}

/*
 * list_is_last - tests whether @list is the last entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static inline int list_is_last(const struct list_head *list, const struct list_head *head)
{
	return list->next == head;
}

/*
 * list_empty - tests whether a list is empty
 * @head: the list to test.
 */
static inline int list_empty(const struct list_head *head)
{
	return head->next == head;
}

/**
 * m_hlist_empty - Is the specified m_hlist_head structure an empty m_hlist?
 * @h: Structure to check.
 */
static inline int m_hlist_empty(const struct m_hlist_head *h)
{
        return !h->first;
}

/*
 * list_entry - get the struct for this entry
 * @ptr:	the &struct list_head pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_struct within the struct.
 */
#define list_entry(ptr, type, member) \
	container_of(ptr, type, member)

/*
 * list_for_each	-	iterate over a list
 * @pos:	the &struct list_head to use as a loop counter.
 * @head:	the head for your list.
 */
#define list_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); \
		pos = pos->next)

/*
 * list_for_each_entry	-	iterate over list of given type
 * @pos:	the type * to use as a loop counter.
 * @head:	the head for your list.
 * @member:	the name of the list_struct within the struct.
 */
#define list_for_each_entry(pos, head, member)				\
	for (pos = list_entry((head)->next, typeof(*pos), member);	\
	     &pos->member != (head); 					\
	     pos = list_entry(pos->member.next, typeof(*pos), member))

/**
 * list_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:	the type * to use as a loop counter.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the list_struct within the struct.
 */
#define list_for_each_entry_safe(pos, n, head, member)			\
	for (pos = list_entry((head)->next, typeof(*pos), member),	\
		n = list_entry(pos->member.next, typeof(*pos), member);	\
	     &pos->member != (head); 					\
	     pos = n, n = list_entry(n->member.next, typeof(*n), member))

#endif

#define m_hlist_entry(ptr, type, member) container_of(ptr,type,member)

#define m_hlist_entry_safe(ptr, type, member) \
	({ typeof(ptr) ____ptr = (ptr); \
	   ____ptr ? m_hlist_entry(____ptr, type, member) : NULL; \
	})

/**
 * m_hlist_for_each_entry - iterate over list of given type
 * @pos:        the type * to use as a loop cursor.
 * @head:       the head for your list.
 * @member:     the name of the m_hlist_node within the struct.
 */
#define m_hlist_for_each_entry(pos, head, member)                         \
	for (pos = m_hlist_entry_safe((head)->first, typeof(*(pos)), member);\
	     pos;                                                       \
	     pos = m_hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

