
#include <stdio.h>
#include <stdarg.h>
#include "printk.h"

void _printk(const char *file, __attribute__((unused)) const char *function, int line, const char *fmt, ...)
{
	int level = LOGL_INFO;
	va_list va;

	/* remove debug level from format */
	if (fmt[0] >= '0' && fmt[0] <= '9') {
		switch (fmt[0]) {
		case '0':
		case '1':
		case '2':
		case '3':
			level = LOGL_ERROR;
			break;
		case '4':
		case '5':
			level = LOGL_NOTICE;
			break;
		case '6':
			level = LOGL_INFO;
			break;
		case '7':
		case '8':
		case '9':
			level = LOGL_DEBUG;
			break;
		}
		fmt++;
	}

	va_start(va, fmt);
	char buffer[4096];
	vsnprintf(buffer, sizeof(buffer), fmt, va);
	LOGPSRC(DMISDN, level, file, line, buffer);
	va_end(va);
}
