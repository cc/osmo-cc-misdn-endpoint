
int mISDN_base_create(void **inst, int protocol);
int mISDN_base_bind(void *inst, struct sockaddr *addr, int addr_len);
int mISDN_base_ioctl(void *inst, unsigned int cmd, void *arg);
int mISDN_base_release(void *inst);

int mISDN_data_create(void **inst, int protocol);
int mISDN_data_getname(void *inst, struct sockaddr *addr, int peer);
int mISDN_data_bind(void *inst, struct sockaddr *addr, int addr_len);
int mISDN_data_ioctl(void *inst, unsigned int cmd, void *arg);
int mISDN_data_release(void *inst);
int mISDN_data_sendmsg(void *inst, unsigned char *data, size_t len, int flags, struct sockaddr_mISDN *maddr);
int mISDN_data_recvmsg(void *inst, unsigned char *data, size_t len, int flags, struct sockaddr_mISDN *maddr);

