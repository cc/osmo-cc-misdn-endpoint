
enum {
	DLCC,
	DOPTIONS,
	DJITTER,
	DISDN,
	DMISDN,
	DDSS1,
	DPH,
};

#define DLCC_DEFINED

extern struct log_info_cat log_categories[];
extern size_t log_categories_size;

